﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace soaprequest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
        string  name="", lat, lon, time;
         
        long id;
        List<long> iList = new List<long>();
        string json="[";


        XmlDocument doc = new XmlDocument();

        private void button1_Click(object sender, EventArgs e)
        {
            InvokeService();
        }

        public void InvokeService()
        {
            //Calling CreateSOAPWebRequest method  
            foreach (long id in iList)
            {


                HttpWebRequest request = CreateSOAPWebRequest();

                XmlDocument SOAPReqBody = new XmlDocument();
                //SOAP Body Request  
                SOAPReqBody.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>  
            <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-   instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">  
             <soap:Body>  
                <GetVehicleDetailsByUnitID xmlns=""http://demo.naqsha.net/"">  
                  
               <UnitID>" + id + "</UnitID><UserKey>c6a7dee1-ceb1-4545-af8a-f58ca0f5e319</UserKey></GetVehicleDetailsByUnitID> </soap:Body></soap:Envelope>");


                using (Stream stream = request.GetRequestStream())
                {
                    SOAPReqBody.Save(stream);
                }
                //Geting response from request  
                string ServiceResult;
                using (WebResponse Serviceres = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                    {
                        //reading stream  

                        ServiceResult = rd.ReadToEnd();
                        //writting stream result on console  
                        textBox4.Text = ServiceResult;

                        doc.LoadXml(ServiceResult);


                        XmlNodeList node1 = doc.DocumentElement.GetElementsByTagName("GetVehicleDetailsByIDResult");

                        foreach (XmlNode item1 in node1)
                        {

                            string name1 = item1["Alias"].InnerText;
                            name1 = name1.Remove(10);
                           // name1 = name1.Remove(4,3);
                            name = name +", "+ name1;
                            DateTime thisDate = DateTime.Parse(item1["RecordDateTime"].InnerText);
                            time = thisDate.ToString("dddd, d MMMM yyy hh:mm:ss tt");
                            lon = item1["LON"].InnerText;
                            lat = item1["LAT"].InnerText;

                            listBox1.Items.Add(name1);
                            listBox2.Items.Add(time);
                            listBox3.Items.Add(lon);
                            listBox4.Items.Add(lat);
                            //json="{\"latitude\":" + lat + ",\"longitude\":" + lon + ",\"poll_time\":\"" + time + "\",\"equipment_nbr\":\"" + name1 + "\"}";
                            json =json+"{\"latitude\":" + lat + ",\"longitude\":" + lon + ",\"poll_time\":\"" + time + "\",\"equipment_nbr\":\"" + name1 + "\"},";

                            //var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://pk-tms.openport.com/GPSDataBridgeAPI/encode/auto/gpsdata");
                            //httpWebRequest.ContentType = "application/json";
                            //httpWebRequest.Method = "POST";

                            //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                            //{
                            //    //json = json.Remove(json.Length - 1);
                            //    //json = json + "]";

                            //    //string json = " {\"latitude\":75.65,\"longitude\":96.54,\"equipment_nbr\":2210}";
                            //    listBox6.Items.Add(json);


                            //    streamWriter.Write(json);
                            //    streamWriter.Flush();
                            //    streamWriter.Close();
                            //}
                            

                            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                            //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                            //{
                            //    var result = streamReader.ReadToEnd();
                            //    listBox5.Items.Add(result);
                            //}




                        }

                    }
                }
                //break;
            }

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://uat-tms.openport.com/GPSDataBridgeAPI/encode/auto/gpsdatalist");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                json = json.Remove(json.Length - 1);
                json = json + "]";

                 //json = " {\"latitude\":75.65,\"longitude\":96.54,\"equipment_nbr\":2210}";
                listBox6.Items.Add(json);


                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                listBox5.Items.Add(result);
            }
            textBox2.Text = listBox2.Items.Count.ToString();
            textBox3.Text = listBox1.Items.Count.ToString();
            textBox5.Text = name;

        }

        public HttpWebRequest CreateSOAPWebRequest()
        {
            //Making Web Request  
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(@"http://203.175.74.156/TestMatrixService/Service.asmx");
            //SOAPAction  
            Req.Headers.Add(@"http://demo.naqsha.net/GetVehicleDetailsByUnitID");
            //Content_type  
            Req.ContentType = "text/xml;charset=\"utf-8\"";
            Req.Accept = "text/xml";
            //HTTP method  
            Req.Method = "POST";
            //return HttpWebRequest  
            return Req;
        }
        public void InvokeService1()
        {
            //Calling CreateSOAPWebRequest method  
            HttpWebRequest request = CreateSOAPWebRequest1();

            XmlDocument SOAPReqBody = new XmlDocument();
            //SOAP Body Request  
            SOAPReqBody.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>  
            <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-   instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">  
             <soap:Body>  
                <GetVehiclesByLogin xmlns=""http://demo.naqsha.net/"">  
                  
                 <UserKey>c6a7dee1-ceb1-4545-af8a-f58ca0f5e319</UserKey> </GetVehiclesByLogin> </soap:Body></soap:Envelope>");


            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            //Geting response from request  
            using (WebResponse Serviceres = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                {
                    //reading stream  
                    string read = rd.ReadToEnd();
                    
                    if (read.StartsWith(_byteOrderMarkUtf8))
                    {
                        read ="<"+ read.Remove(0, _byteOrderMarkUtf8.Length);
                    }
                    //writting stream result on console  
                    richTextBox1.Text = read;

                    doc.LoadXml(read);
                    
                 
                    XmlNodeList node = doc.DocumentElement.GetElementsByTagName("GetVehiclesByLogin_TestResult");
                   
                    foreach (XmlNode item in node)
                    {
                        
                        //string vehicle=item["Alias"].InnerText;
                        //vehicle = vehicle.Remove(10);
                        //listBox1.Items.Add(vehicle);
                        iList.Add(long.Parse(item["UnitMasterID"].InnerText));

                        
                       
                        
                       
                    }

                    
                }
            }
            InvokeService();
        }

        public HttpWebRequest CreateSOAPWebRequest1()
        {
            //Making Web Request  
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(@"http://203.175.74.156/TestMatrixService/Service.asmx");
            //SOAPAction  
            Req.Headers.Add(@"http://demo.naqsha.net/GetVehiclesByLogin");
            //Content_type  
            Req.ContentType = "text/xml;charset=\"utf-8\"";
            Req.Accept = "text/xml";
            //HTTP method  
            Req.Method = "POST";
            //return HttpWebRequest  
            return Req;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            InvokeService1();
        }
    }
}
